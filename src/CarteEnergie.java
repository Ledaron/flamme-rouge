

public class CarteEnergie extends Carte{
	
	
	/**
	 * Constructeur de la classe CarteEnergie
	 * @param f	Valeur de la CarteEnergie
	 */
	CarteEnergie(int f){
		super(f);
	}
	
	
	/**
	 * @return une cha�ne de caract�re donnant des informations sur la CarteEnergie
	 *
	 */
	public String toString(){
		return "Carte Energie de valeur: " + this.getVal();
	}

}

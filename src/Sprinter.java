import java.util.Collections;


public class Sprinter extends Cycliste {
	
	/**
	 * Cosntructeur classe Sprinter
	 * @param p La position du pion sprinter
	 */
	public Sprinter(int p){
		super(p);
	}
	
	/**
	 * Methode qui permet de remplir l'attribut Deck de l'instance
	 */
	public void remplirPaquet(){
		CarteEnergie[] tab = new CarteEnergie[15];

		for(int i =0; i < 3; i++){
			
			tab[i] = new CarteEnergie(2);
			tab[i + 3] = new CarteEnergie(3);
			tab [i + 6] = new CarteEnergie(4);
			tab [i + 9] = new CarteEnergie(5);
			tab [i + 12] = new CarteEnergie(9);
		}
		for(int j = 0; j < 15; j ++){
			this.Cartes.add(tab[j]);
		}
		Collections.shuffle(this.Cartes);
	}
	
	
	/**
	 * @return une cha�ne de caracteres donnant l'insformation sur le type de l'instance
	 */
	public String MonType() {
		return "S";
	}
	
	
	/**
	 * @return une cha�ne de caracteres donnant des informations sur l'instance
	 */
	public String toString(){
		String r = "Sprinter � la place " + this.getPlace();
		return r;
	}
}



public class CarteFatigue extends Carte {

		public final static int DEFAULT_VAL = 2;
		
		
		
		/**
		 * Constructeur de la classe CarteFatigue
		 * @param f Valeur de la CarteFatigue
		 */
		CarteFatigue(int f){
			super(f);
		}
		
		
		/**
		 * @return une cha�ne de caract�re donnant des informations sur la CarteFatigue
		 */
		public String toString(){
			return "Carte Fatigue de valeur: " + this.getVal();
		}
}

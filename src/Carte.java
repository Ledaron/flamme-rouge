

public abstract class Carte {
	
	private int val;
	
	/**
	 * Constructeur de la class Carte, initialise l'attribue valeur
	 * @param d La valeur de la Carte 
	 *
	 */
	Carte(int d){
		this.val = d;
	}
	
	
	/**
	 * @return la valeur de la Carte
	 *
	 */
	int getVal(){
		return this.val;
	}
	
	
	
	/**
	 * Change la valeur de la Carte
	 * @param d Nouvelle valeur de la Carte
	 *
	 */
	void setVal(int d){
		this.val = d;
	}
	
	
	
	/**
	 * @return une cha�ne de caract�re donnant des informations sur la Carte
	 */
	public String toString(){
		return "Carte de valeur: " + this.getVal();
	}

	
}

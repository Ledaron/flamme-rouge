
public class Parcours {
	String nom;
	int taille;	
	int[] relief;	
	String[] terrain;	
	String[][] StatutParcours;	
	
	/**
	 * constructeur classe Parcours
	 * @param t
	 * @param n
	 */

	public Parcours(int t,String n){
		this.taille = t;
		this.nom = n;
		this.terrain = new String[t];
		for (int i=0;i<this.terrain.length;i++) {
			this.terrain[i]="Plat";
		}
		this.relief = new int[t];
		this.StatutParcours = new String[2][t];
		for (int i=0;i<this.StatutParcours.length;i++) {
			for(int j=0;j<this.StatutParcours[0].length;j++) {				
				this.StatutParcours[i][j]="NN";
			}
		}
		this.ajoutLigneDepartArriver();
	}
	
	/**
	 * Methode permettant d'ajouter la ligne de depart et la ligne d'arrivee
	 * 
	 * @throws ParcoursException quand la taille du parcours est trop petit
	 * 
	 */
	public void ajoutLigneDepartArriver() {
		try{
			if (this.terrain.length<10) {
				throw new ParcoursException("Le parcours est trop petit");
			}
			for (int i=0;i<5;i++) {
				this.terrain[i]="Depart";
				this.terrain[this.terrain.length-i-1]="Arrive";
			}
		}catch (ParcoursException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Methode permettant de determiner si une case a l'indexe est une case d�part
	 * @param indexe
	 * @return boolean
	 */
	public boolean auDepart(int indexe) {
		if (this.terrain[indexe]=="Depart")return true;
		return false;
	}
	
	
	/**
	 * Methode permettant de determiner si une case a l'indexe est une case depart 
	 * * @param	 indexe 
	 * @return boolean
	 */
	public boolean estArrive(int indexe) {
		if (this.terrain[indexe]=="Arrive")return true;
		return false;
	}
	
	
	/**
	 * Methode qui permet d'ajouter un virage
	 * @param 	boolean serre 
	 * @param	int indiceDebut : d�but du virage
	 * @param   int longueur : longueur du virage
	 */
	public void Virage(boolean serre,int indiceDebut,int longueur) {
		try{
			if (serre && longueur>4) {
				throw new ParcoursException("Votre virage est trop serre");
			}else if (this.auDepart(indiceDebut) || this.estArrive(indiceDebut+longueur)) {
				throw new ParcoursException("les lignes d'arriv�e ou de d�part ne peuvent �tre des virages");
			}else if (!serre && longueur>8){
				throw new ParcoursException("Votre virage est trop large");
			}
			for(int i =indiceDebut-1;i<=indiceDebut+longueur-2;i++) {
				if(serre) {
					this.terrain[i]="VirageSerre";
				}else {
					this.terrain[i]="VirageLeger";
				}
			}
		}catch (ParcoursException e) {
			e.printStackTrace();
		}catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Le virage que vous voulez inserer sort du parcours");
		}
	}
	
	
	/**
	 * Methode permettant de determiner si une case � l'indexe est un virage
	 * * @param indexe 
	 * @return boolean
	 */
	public boolean estUnVirage(int indexe) {
		if (this.terrain[indexe]=="VirageSerre" || this.terrain[indexe]=="VirageLeger")return true;
		return false;
	}
	
	
	/**
	 * Methode permettant de d�terminer si une case � l'indexe est un virage Serre
	 * * @param	 indexe 
	 * @return boolean
	 */
	public boolean estVirageSerre(int indexe) {
		if (this.estUnVirage(indexe) && this.terrain[indexe]=="VirageSerre")return true;
		return false;
	}
	
	
	/**
	 * Methode permettant de d�terminer si une case � l'indexe est un virage Leger
	 * * @param	indexe 
	 * @return boolean
	 */
	public boolean estVirageLeger(int indexe) {
		if (this.estUnVirage(indexe) && this.terrain[indexe]=="VirageLeger")return true;
		return false;
	}
	
	 
	/**
	 * Methode qui permet d'ajouter du relief au parcour 
	 * @param int indiceDebut : d�part de la pente
	 * @param int longueur : longueur de la pente
	 * @param char typeRelief
	 */
	public void addRelief(int indiceDebut,int longueur,char typeRelief) {
		try{
			if (this.auDepart(indiceDebut) || this.estArrive(indiceDebut+longueur)) {
				throw new ParcoursException("les lignes d'arriv�e ou de d�part ne peuvent �tre une montagne");
			}
			for (int i=indiceDebut-1;i<indiceDebut+longueur-1;i++) {
				switch(typeRelief) {
				case 'M':
					this.relief[i]=1;
					break;
				case 'D':
					this.relief[i]=2;
					break;
				default:
					System.out.println("Erreur : type inconnu");
					break;
				}
			}
		}catch (ParcoursException e) {
			e.printStackTrace();
		}catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Le relief que vous voulez inserer sort du parcours");
		}
	}
	/**
	 * Methode permettant de d�terminer si une case � l'indexe est une mont�e
	 * * @param	 indexe 
	 * @return boolean
	 */
	public boolean estMontee(int indexe) {
		if (this.relief[indexe]==1)return true;
		return false;
	}
	
	/**
	 * Methode permettant de d�terminer si une case � l'indexe est une descente
	 * * @param indexe 
	 * @return boolean
	 */
	public boolean estDescente(int indexe) {
		if (this.relief[indexe]==2)return true;
		return false;
	}
	
	
	/**
	 * Methode qui permet d'ajouter une montee puis une descente ou l'inverse
	 * @param  int indiceDebut : debut de la pente
	 * @param  boolean monterFirst 
	 * @param  int longueur1 : longueur de la premi�re pente
	 * @param  int longueur2 : longueur de la seconde pente
	 */
	public void Montee(int indiceDebut,boolean monterFirst,int longueur1,int longueur2) {
		try{
			if (this.auDepart(indiceDebut) || this.estArrive(indiceDebut+longueur1+longueur2)) {
				throw new ParcoursException("les lignes d'arriv�e ou de d�part ne peuvent �tre une montagne");
			}
			for (int i=indiceDebut-1;i<indiceDebut+longueur1-1;i++) {
				if (monterFirst) {
					this.relief[i]=1;
				}else {
					this.relief[i]=2;
				}
			}
			for (int i=indiceDebut+longueur1-1;i<indiceDebut+longueur1+longueur2-1;i++) {
				if (monterFirst) {
					this.relief[i]=2;
				}else {
					this.relief[i]=1;
				}
			}
		}catch (ParcoursException e) {
			e.printStackTrace();
		}catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Le relief que vous voulez inserer sort du parcours");
		}
	}
	
	/**
	 * Methode qui initialise les places des joueurs dans le parcous
	 * @param int[][] tab 
	 */
	public void depart_ini(String[][] tab) {
		for (int i=0;i<tab.length;i++) {
			for (int j=0;j<tab[0].length;j++) {
				this.StatutParcours[i][j]=tab[i][j];
			}
		}
	}
	
	
	public int[] getRelief() {
		return this.relief;
	}
	
	public String[][] getStats() {
		return this.StatutParcours;
	}
	
	public String[] getTerrain(){
		return this.terrain;
	}
	
	/**
	 * Methode qui affiche l'�tat d'un objet parcours 
	 */
	public String toString() {
		String ligne="",ligne2="",relief="",terrain = "";
		for (int i=0;i<this.StatutParcours[0].length;i++) {
			ligne +=this.StatutParcours[0][i]+" ";
			ligne2 +=this.StatutParcours[1][i]+" ";
			relief +=this.relief[i]+" ";
			terrain +=this.terrain[i]+" ";
		}
		return ligne+"\n"+ligne2+"\n"+terrain+"\n"+relief+"\n";
	}
	
}
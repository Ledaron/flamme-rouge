import java.util.ArrayList;
import java.util.Scanner;


public class Game {
	
	
	/**
	 * Constructeur par d�faut class Game
	 */
	public Game() {
	}
	
	
	/**
	 * Constructeur classe CarteEnergie
	 */
	public void startGame() {
	
		Parcours p = new Parcours(28, "lal");
		p.Virage(false, 10, 4);
		p.Virage(true, 17, 2);
		p.addRelief(7, 4, 'M');
		p.addRelief(13, 2, 'D');
		p.Montee(16, false, 2, 3);

		Scanner sc = new Scanner(System.in);
		System.out.print("Entre le nombre de joueurs: ");
		int nbJ = sc.nextInt();
		
		while (nbJ < 2 || nbJ > 4) {
			
			System.out.print("Nombre de joueurs incorrect.");
			System.out.print("R�entrer un nombre de joueurs: ");
			nbJ = sc.nextInt();
		}
		ArrayList<Joueur> l_joueurs = new ArrayList<Joueur>();
		String[][] placeDepart = new String[2][5];
		
		for (int i=0;i<placeDepart.length;i++) {
			
			for(int j=0;j<placeDepart[0].length;j++) {				
				placeDepart[i][j]="NN";
			}
		}
		for (int i = 0; i < nbJ; i++) {
			System.out.print("Quel est votre pseudo Joueur" + (i + 1) + " ? ");
			String nomJoueur = sc.next();
			System.out.print("Quel est votre pseudo pour l'equipe " + nomJoueur + " ? ");
			String nomEquipe = sc.next();
			
			while (Outils.nomExistant(l_joueurs, nomEquipe)) {
				System.out.println("Ce nom d'�quipe est d�j� prit, veuillez en choisir un autre");
				nomEquipe = sc.next();
			}
			Outils.afficheTab(placeDepart);
			
			System.out.println("A quel emplacement voulez vous placer votre Sprinter (choix entre 1 et 5) ? ");
			
			int placeSprinter = sc.nextInt();
			Sprinter spt = new Sprinter(placeSprinter);
			Outils.ajCoordIntab(placeDepart, placeSprinter, i + 1, p,spt);
			Outils.afficheTab(placeDepart);
			
			System.out.println("A quel emplacement voulez vous placer votre Rouleur (choix entre 1 et 5) ? ");
			int placeRl = sc.nextInt();
			
			Rouleur rouleur = new Rouleur(placeRl);
			Outils.ajCoordIntab(placeDepart, placeRl, i + 1, p,rouleur);
			l_joueurs.add(new Joueur(nomJoueur, nomEquipe, spt, rouleur));
		}
		p.depart_ini(placeDepart);
		Outils.Terrain(p);
	}
	
	
	
}

import java.util.ArrayList;
import java.util.Collections;



public abstract class Cycliste {
	
	private int place;
	protected ArrayList<Carte> Cartes;
	protected ArrayList<Carte> CartesVisibles;
	
	
	/**
	 * Constructeur de la class Cycliste
	 * @param p	donne la position du cycliste
	 */
	public Cycliste(int p){
		this.place = p;
		this.Cartes = new ArrayList<Carte>();
		this.CartesVisibles = new ArrayList<Carte>();
	}
	
	/**
	 * Methode abstract qui remplit l'attribut Paquet de class
	 */
	public abstract void remplirPaquet();
	
	/**
	 * Methode abstract
	 * @return une cha�ne de caract�re donnant le type de l'instance
	 */
	public abstract String MonType();
	
	
	/**
	 * Methode permettant au cycliste d'avancer sur le circuit
	 * @param av   Le nombre de cases que le cycliste doit avancer
	 */
	public void avancer(int av){
		this.place += av;
	}
	
	/**
	 * @return la position du cycliste
	 *
	 */
	public int getPlace(){
		return this.place;
	}
	
	
	/**
	 * Methode permettant de piocher dans l'attribut Paquet de la class
	 * @return une liste de Carte contenant la pioche
	 */
	public ArrayList<Carte> piocher(){
		ArrayList<Carte> r = new ArrayList<Carte>();
		if(this.Cartes.size() < 4){
			while(!this.Cartes.isEmpty()){
				r.add(this.Cartes.get(0));
				this.Cartes.remove(0);
			}
			while(!CartesVisibles.isEmpty()) {
				this.Cartes.add(CartesVisibles.get(0));
				CartesVisibles.remove(0);
			}
			Collections.shuffle(this.Cartes);
			while(r.size() != 4) {
				r.add(this.Cartes.get(0));
				this.Cartes.remove(0);
			}
		}
		else{
			for(int i = 0; i < 4; i++){
				r.add(this.Cartes.get(0));
				this.Cartes.remove(0);
			}
		}
		return r;
	}
	
	
	/**
	 * Methode permettant de choisir quelle carte on souhaite jouer entre les 4 cartes que nous avions piocher
	 * @param pioche, i			
	 * @return carte choisi
	 */
	public Carte CarteAJouer(ArrayList<Carte> pioche, int i){
		Carte r = pioche.get(i);
		ArrayList<Carte> cartesADef = pioche;
		cartesADef.remove(i);
		while(!cartesADef.isEmpty()){
			this.CartesVisibles.add(cartesADef.get(0));
			cartesADef.remove(0);
		}
		return r;
	}
	
	
	/**
	 * Methode permettant d'afficher la liste des Cartes contenues dans l'attribut Paquet de la class
	 */
	public void  Cartes(ArrayList<Carte> p){
		String r = "";
		for(int i = 0; i < p.size(); i++){
			r = r + "Carte " + i + ": " + p.get(i) + "\n";
		}
		System.out.println(r);
	}
}

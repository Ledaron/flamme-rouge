/**
 * Class ParcoursException renvoyant un meesage en cas d'erreur
 */
public class ParcoursException extends Exception {
	
	
	/**
	 * Constructeur classe ParcoursException
	 */
	public ParcoursException(String s) {
		super(s);
	}
}

public class Joueur {
	private String nom;
	private String nomEquipe;
	private Sprinter sprinter;
	private Rouleur rouleur;
	
	
	/**
	 * Constructeur de la class Joueur
	 * @param id, nom, nomeq, sp, rl
	 * 			valeur de l'idJouer
	 * 			nom du joueur
	 * 			nom de l'equipe du joueur
	 * 			le pion sprinter du joueur
	 * 			le pion rouleur du joueur
	 */
	public Joueur(String nom, String nomeq, Sprinter sp, Rouleur rl){
		this.nom = nom;
		this.nomEquipe = nomeq;
		this.sprinter = sp;
		this.rouleur = rl;
	}
	
	
	/**
	 * @return le nom du joueur
	 */
	public String getNom(){
		return this.nom;
	}
	
	/**
	 * @return le nom de l'equipe du joueur
	 */
	public String getNomEquipe(){
		return this.nomEquipe;
	}
	
	
	/**
	 * @return le pion sprinter du joueur
	 */
	public Sprinter getSprinter(){
		return this.sprinter;
	}
	
	
	/**
	 * @return le pion rouleur du joueur
	 */
	public Rouleur getRouleur(){
		return this.rouleur;
	}
	
	
	/**
	 * @return une cha�ne de caractere contenant des informations sur le joueur
	 */
	public String toString(){
		return this.getNom() + "de l'equipe: " + this.getNomEquipe();
	}
	
	
}

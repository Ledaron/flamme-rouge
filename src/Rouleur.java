import java.util.Collections;


public class Rouleur extends Cycliste{
	
	
	/**
	 * Constructeur classe Rouleur
	 * @param p Valeur de la position du pion Rouleur
	 */
	public Rouleur(int p){
		super(p);
	}
	
	
	/**
	 * Methode permettant de remplir l'attribut Deck de l'instance
	 */
	public void remplirPaquet(){
		CarteEnergie[] tab = new CarteEnergie[15];
		
		for(int i =0; i < 3; i++){
			
			tab[i] = new CarteEnergie(3);
			tab[i + 3] = new CarteEnergie(4);
			tab [i + 6] = new CarteEnergie(5);
			tab [i + 9] = new CarteEnergie(6);
			tab [i + 12] = new CarteEnergie(7);
		}
		for(int j = 0; j < 15; j ++){
			
			this.Cartes.add(tab[j]);
		}
		Collections.shuffle(this.Cartes);
	}
	
	
	/**
	 * @return une cha�ne de caract�res donnant l'insformation sur le type de l'instance
	 */
	public String MonType() {
		return "R";
	}
	
	
	/**
	 * @return une cha�ne de caract�res donnant des informations sur l'instance
	 */
	public String toString(){
		String r = "Rouleur � la position " + this.getPlace();
		return r;
	}
}

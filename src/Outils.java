import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;


public class Outils {
	
	
	/**
	 * Methode permettant d'afficher les places
	 */
	public final static void afficheTab(String[][] placeDepart) {
		String place="",ligne1="",ligne2="";
		for(int i=0;i<placeDepart[0].length;i++) {
			place +=(i+1)+"  ";
			ligne1 +=placeDepart[0][i]+" ";
			ligne2 +=placeDepart[1][i]+" ";	
		}
		System.out.println("Position "+place + "Depart");
		System.out.println("Gauche   "+ligne1 + "Depart");
		System.out.println("Droite   "+ligne2 + "Depart");
	}
	
	public final static void ajCoordIntab(String[][] placeDepart,int pos,int idjoueur,Parcours p,Cycliste c) {
		Scanner sc = new Scanner(System.in);
		
		while (!p.auDepart(pos-1)) {
			
			System.out.println("Le cycliste doit �tre plac� que sur les places de d�part");
			System.out.println("Les cases d�part correspondent � la case 1 � la case 5");
			pos = sc.nextInt();
		}
		while(placeDepart[0][pos-1]!="NN" && placeDepart[1][pos-1]!="NN") {
			
			System.out.println("Place d�j� occup�e (veuillez choisir une place avec un 0)");
			afficheTab(placeDepart);
			pos = sc.nextInt();
		}
		if (placeDepart[1][pos-1]!="NN") {
			placeDepart[0][pos-1]=(c.MonType()+idjoueur);
		}else {
			placeDepart[1][pos-1]=(c.MonType()+idjoueur);
		}
	}
	
	public static boolean nomExistant(ArrayList<Joueur> l,String nom) {
		Iterator<Joueur> i = l.iterator();
		
		while(i.hasNext()) {
			
			if (i.next().getNomEquipe().equals(nom)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 *m�thode permmettant d'afficher le terrain
	 * @param p 
	 */
	public static void Terrain(Parcours p) {
		int[] relief = p.getRelief();
		String[][] pJoueurs = p.getStats();
		String[] terrain = p.getTerrain();
		String l1="|",l2="|",l3="|",l4="|",l5="|",l6="|";
		
		for (int i=0;i<relief.length;i++) {
			l1 +="===";
			l6 +="===";
			switch(terrain[i]) {
			case "Depart":
				l2+="DP|";
				l5+="DP|";
				break;
			case "Arrive":
				l2+="AR|";
				l5+="AR|";
				break;
			case "VirageLege":
				l2+="VL|";
				break;
			case "VirageSerre":
				l2+="VS|";
				break;
			default:
				l2+="==|";
				break;
			}
			if(relief[i]==1) {
				l5+="MO|";
			}else if (relief[i]==2) {
				l5+="DE|";
			}else{
				if (!(terrain[i].equals("Depart") || terrain[i].equals("Arrive"))){
					l5+="==|";
				}
			}
			switch(pJoueurs[0][i]) {
			case "R1":
				l3+="R1|";
				break;
			case "R2":
				l3+="R2|";
				break;
			case "R3":
				l3+="R3|";
				break;
			case "R4":
				l3+="R4|";
				break;
			case "S1":
				l3+="S1|";
				break;
			case "S2":
				l3+="S2|";
				break;
			case "S3":
				l3+="S3|";
				break;
			case "S4":
				l3+="S4|";
				break;
			default:
				l3+="  |";
				break;
			}
			
			switch(pJoueurs[1][i]) {
			case "R1":
				l4+="R1|";
				break;
			case "R2":
				l4+="R2|";
				break;
			case "R3":
				l4+="R3|";
				break;
			case "R4":
				l4+="R4|";
				break;
			case "S1":
				l4+="S1|";
				break;
			case "S2":
				l4+="S2|";
				break;
			case "S3":
				l4+="S3|";
				break;
			case "S4":
				l4+="S4|";
				break;
			default:
				l4+="  |";
				break;
			}
			
		}
		System.out.println(l1+"\n"+l2+"\n"+l3+"\n"+l4+"\n"+l5+"\n"+l6);
	}
	

}
